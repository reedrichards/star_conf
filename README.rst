=========
Star Conf
=========


.. image:: https://img.shields.io/pypi/v/star_conf.svg
        :target: https://pypi.python.org/pypi/star_conf
        :alt: pypi

.. image:: https://gitlab.com/reedrichards/star_conf/badges/master/coverage.svg?job=coverage
        :target: https://reedrichards.gitlab.io/star_conf/coverage/index.html
        :alt: coverage

.. image:: https://gitlab.com/reedrichards/star_conf/badges/master/pipeline.svg
        :alt: pipeline

.. image:: https://img.shields.io/pypi/l/star_conf.svg
        :target: https://gitlab.com/reedrichards/star_conf/raw/master/LICENSE
        :alt: PyPI - License

.. image:: https://img.shields.io/pypi/dm/star_conf.svg
        :alt: PyPI - Downloads

.. image:: https://img.shields.io/pypi/pyversions/star_conf.svg
        :alt: PyPI - Python Version

.. image:: https://img.shields.io/pypi/status/star_conf.svg
        :alt: PyPI - Status



Use Starlette's configuration as a standalone package


* Free software: BSD License
* Documentation: https://reedrichards.gitlab.io/star_conf/index.html
* Coverage: https://reedrichards.gitlab.io.gitlab.io/star_conf/coverage/index.html
* Gitlab: https://gitlab.com/reedrichards/star_conf


`starlette <https://www.starlette.io/>` is a lightweight ASGI framework/toolkit, which is ideal for building high performance asyncio services.
It has some datastructures used for configuration that I really like using in my other projects, but I don't want to bring in the entire starlette project as a dependency. Thus this project was born.

Quickstart
----------

**Install**

.. code-block:: console

    $ pip install star_conf


**Usage**


use ``Secret``

    >>> secret_string = Secret('keepmeasecret')
    >>> secret_string
    Secret('**********')
    >>> print(secret_string)
    keepmeasecret

Config will be read from environment variables and/or ".env" files.

    >>> config = Config(".env")
    >>> DEBUG = config('DEBUG', cast=bool, default=False)
    >>> SECRET_KEY = config('SECRET_KEY', cast=Secret)
    >>> # not implemented yet
    >>> ALLOWED_HOSTS = config('ALLOWED_HOSTS', cast=CommaSeparatedStrings)

**.env**

.. code-block:: txt

    # Don't commit this to source control.
    # Eg. Include ".env" in your `.gitignore` file.
    DEBUG=True
    SECRET_KEY=43n080musdfjt54t-09sdgr
    ALLOWED_HOSTS=127.0.0.1, localhost
