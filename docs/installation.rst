.. highlight:: shell

============
Installation
============


Stable release
--------------

To install Star Conf, run this command in your terminal:

.. code-block:: console

    $ pip install star_conf

This is the preferred method to install Star Conf, as it will always install the most recent stable release.

If you don't have `pip`_ installed, this `Python installation guide`_ can guide
you through the process.

.. _pip: https://pip.pypa.io
.. _Python installation guide: http://docs.python-guide.org/en/latest/starting/installation/


From sources
------------

The sources for Star Conf can be downloaded from the `Gitlab repo`_.

You can either clone the public repository:

.. code-block:: console

    $ git clone git://gitlab.com/reedrichards/star_conf

Or download the `tarball`_:

.. code-block:: console

    $ curl -OJL https://github.com/reedrichards/star_conf/tarball/master

Once you have a copy of the source, you can install it with:

.. code-block:: console

    $ python setup.py install


.. _Github repo: https://gitlab.com/reedrichards/star_conf
.. _tarball: https://gitlab.com/reedrichards/star_conf/tarball/master
