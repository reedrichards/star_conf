star\_conf package
==================

Submodules
----------

star\_conf.datastructures module
--------------------------------

.. automodule:: star_conf.datastructures
    :members:
    :undoc-members:
    :show-inheritance:


Module contents
---------------

.. automodule:: star_conf
    :members:
    :undoc-members:
    :show-inheritance:
