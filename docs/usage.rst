=====
Usage
=====

To use Star Conf in a project::

use ``Secret``

    >>> secret_string = Secret('keepmeasecret')
    >>> secret_string
    Secret('**********')
    >>> print(secret_string)
    keepmeasecret

Config will be read from environment variables and/or ".env" files.

    >>> config = Config(".env")
    >>> DEBUG = config('DEBUG', cast=bool, default=False)
    >>> SECRET_KEY = config('SECRET_KEY', cast=Secret)
    >>> # not implemented yet
    >>> ALLOWED_HOSTS = config('ALLOWED_HOSTS', cast=CommaSeparatedStrings)

**.env**

.. code-block:: txt

    # Don't commit this to source control.
    # Eg. Include ".env" in your `.gitignore` file.
    DEBUG=True
    SECRET_KEY=43n080musdfjt54t-09sdgr
    ALLOWED_HOSTS=127.0.0.1, localhost
